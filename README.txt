-- SUMMARY --

This is a very simple module that will demo integration of Drupal with Microsoft Dynamics CRM online.
Once installed will create a demo content type called Contact CRM. If a record is created then is propagated to the CRM.
Node id and GUID(unique id from the CRM) is stored on the database.
The code it's suppose to be a starting point for someone that wants to integrate Drupal with MS Dynamics CRM online.
One can create a content type using CCK and map all the custom fields to be propagated to the CRM using the dynamics_crm_nodeapi function.
The Contact CRM will only show the title value. The rest of the fields are updated in the CRM and stored in the database under table name 'dynamics_crm_nodes'.
As this module is more like an example I didn't take the time to develop more. Any co-maintainers are welcomed.

-- REQUIREMENTS --

Step 1: Apply for an Demo account for Microsoft Dynamics CRM

Once you've created an account then you should be presented on the screen with an url for account.
For example: "https://test.crm.dynamics.com/"
Once you visit this url and you are logged in with your demo user then you should be able to view your Dynamics dashboard.
By default there are few types of objects (entities) already created for you. For example: "Account" and "Contact".

Step 2: Download the Dynamics Integration Library for PHP

This Library was developed by Zbynek Pavelek (zbynek.pavelek@gmail.com).
Zbynek helped us to develop this module for integration with MS Dynamics. 
We were told that the library(connection bridge) was not made open source by Zbynek yet,
but it's free for anyone to use as long as the Copyright is still under his name. 
The files can be downloaded for free here:

http://www.verticalwavesolutions.com/dynamics_crm/php_library.zip

For any inquiries about the Copyright of this library please contact Zbynek Pavelek 
email: zbynek.pavelek@gmail.com
website: http://www.zenithies.org


-- CONFIGURATION --

Customize the settings in Administer >> Site configuration >> MS Dynamics CRM settings



-- CUSTOMIZATION --

Developers can change this function "dynamics_crm_nodeapi" and add any content type that they want to update.



-- CONTACT --

Current maintainers:

* Alex Holetec  http://drupal.org/user/307770


This project has been sponsored by:

* VerticalWave Solutions -- http://verticalwavesolutions.com

  We are a web development company based in Toronto, Ontario that offers high-end web solutions and services.
  Our solutions, based on the popular Drupal CMS, help companies be more productive, communicate and interact better.
  Corporate social networks, intranet/extranet, online collaboration, community portals, e-commerce or just a new presentation website,
  VerticalWave Solutions has the skills and experience to deliver successful projects for any size organization.


-- EXAMPLE --


/**
* Just an example
* creates an Account and updates it. 
*/
function dynamics_crm_create_and_update_account()
{
	try {
	    
	    $bridge->connnect();
	    
	    // Prepare your accounts fields to import, account is here represented as associative array with pairs
	    // key => $value ie 'firstname' => 'John' etc
	    
	    $newAccount = array(
	        'name' => 'test',
	        'emailaddress1' => 'test@test.com',
	        'telephone1' => '+420 000 000 000 000',
	        // etc ..
	    );
	    
	    // This will send account into crm
	    $newAccountId = $bridge->pushAccount($newAccount);
	    
	    // Now you have new account ID guid in $newAccountId, you can use it for update               
	    $updateAccount = array(
	        // Primary key GUID id must be presented
	        'accountid' => $newAccountId,
	        // Only fields presented in associative array will be updated
	        'telephone2' => '+420 111 111 111 111',
	    );
	    
	    // Perform update
	    $bridge->updateAccount($updateAccount);
	    
	    // You can also export existing accounts from MSCRM and filter them
	    $filter = new MscrmFilter();
	    $filter->addCondition('accountid', $newAccountId);
	
	    $accounts = $bridge->pullAccounts($filter);
	    
	    // You can find another entities description here:
	    // http://technet.microsoft.com/en-us/library/aa683648.aspx
	    // Also checkout MscrmExt class code where you can define any other agenda to push/pull/update
	    // using simpleUpdate, simplePush, simplePull methods        
	    
	
	} 
	catch (Exception $e) {
		// If something go wrong tries to give you info
	    drupal_set_message('<h1>Error:</h1>');
		drupal_set_message($e->getMessage());
		drupal_set_message($bridge->dump(true));
	}
}
