<?php

/**
* Configure admin interface.
*
* @ingroup forms
* @see system_settings_form().
*/
function dynamics_crm_admin_settings() 
{
	$form['dynamics_crm_user'] = array(
		'#type' => 'textfield',
		'#title' => t('User name'),
		'#description' => t('Microsoft Dynamics Username'),
		'#default_value' => variable_get('dynamics_crm_user', 'user'),
		'#size' => 50
	);
	
	$form['crm_user_passwd'] = array(
		'#type' => 'password',
		'#title' => t('Password'),
		'#description' => t('Users\'s Password'),
		'#default_value' => variable_get('dynamics_crm_passwd', 'password'),
		'#size' => 50
	);
	
	$form['crm_discovery_host'] = array(
		'#type' => 'textfield',
		'#title' => t('Discovery Host'),
		'#description' => t('Microsoft Dynamics Discovery Host name'),
		'#default_value' => variable_get('dynamics_crm_discovery_host', 'dev.crm.dynamics.com'),
		'#size' => 50
	);
	
	$form['dynamics_crm_address'] = array(
		'#type' => 'textfield',
		'#title' => t('CRM Address'),
		'#description' => t('Microsoft Dynamics CRM Address'),
		'#default_value' => variable_get('dynamics_crm_address', 'test.crm.dynamics.com'),
		'#size' => 50
	);
	
	$form['dynamics_crm_host'] = array(
		'#type' => 'textfield',
		'#title' => t('CRM Host'),
		'#description' => t('Microsoft Dynamics Host name'),
		'#default_value' => variable_get('dynamics_crm_host', 'test.api.crm.dynamics.com'),
		'#size' => 50
	);
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save')
	);
	
	return $form;
}


/**
* Validate the annotation configuration form.
*/
function dynamics_crm_admin_settings_validate($form, &$form_state) 
{
	if ($form_state['values']['dynamics_crm_user'] == '') {
		form_set_error('', t('Please enter the username.'));
	}
	if ($form_state['values']['crm_user_passwd'] == '') {
		form_set_error('', t('Please enter the password.'));
	}
	if ($form_state['values']['dynamics_crm_host'] == '') {
		form_set_error('', t('Please enter the Dynamics Host name.'));
	}
}


/*
* save settings in Drupal variables
*/
function dynamics_crm_admin_settings_submit($form, &$form_state) 
{
	variable_set('dynamics_crm_user',$form_state['values']['dynamics_crm_user']);
	variable_set('dynamics_crm_passwd',$form_state['values']['crm_user_passwd']);
	variable_set('dynamics_crm_discovery_host',$form_state['values']['crm_discovery_host']);
	variable_set('dynamics_crm_address', $form_state['values']['dynamics_crm_address']);
	variable_set('dynamics_crm_host', $form_state['values']['dynamics_crm_host']);
	drupal_set_message('Your Microsoft Dynamics settings have been saved.');
}




