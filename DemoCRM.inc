<?php

module_load_include('php', 'dynamics_crm', 'lib/Mscrm4Online');

/**
* Class extension for Demo
* @author Alex Holetec - VerticalWave Solutions.
*/
class DemoCRM extends Mscrm4Online
{
		/*
		 * http://technet.microsoft.com/en-us/library/aa680066.aspx		-- Account class
		 * Sends account into CRM, returns GUID id of newly created record
		 */
        public function pushAccount($account)
        {
            return $this->simplePush($account, 'account');
        }
		
		/*
		 * http://technet.microsoft.com/en-us/library/aa681095.aspx		-- Contact class
		 * Sends contact into CRM, returns GUID id of newly created record
		 */
        public function pushContact($contact)
        {
            return $this->simplePush($contact, 'contact');
        }
		
		/*
		 * http://technet.microsoft.com/en-us/library/aa682581.aspx		-- Lead class
		 * Sends a new Lead into the CRM, returns GUID id of newly created record
		 */
        public function pushLead($lead)
        {
            return $this->simplePush($lead, 'lead');
        }		
	
        // Updates account with given 'accountid' => GUID id
        public function updateAccount($account)
        {
            return $this->simpleUpdate($account, 'account');
        }
		
		// Updates contact with given 'contactid' => GUID id
        public function updateContact($contact)
        {
            return $this->simpleUpdate($contact, 'contact');
        } 
        
        // Returns all accounts accesible by connected user
        public function pullAccounts($filter = null)
        {
            return $this->simplePull('account', $filter);
        }   
		
		// Returns all Contacts
        public function pullContacts($filter = null)
        {
            return $this->simplePull('contact', $filter);
        }
		
		// Returns all Leads
        public function pullLeads($filter = null)
        {
            return $this->simplePull('lead', $filter);
        } 
		
		// Delete a lead
        public function deleteLead($guid = null)
        {
			return $this->simpleDelete(array('leadid' => $guid), 'lead'); 
        }  
		
}
